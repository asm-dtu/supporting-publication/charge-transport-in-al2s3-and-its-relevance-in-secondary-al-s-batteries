#!/bin/bash
# © [2021] Technical University of Denmark
#
# Obtain the data and set up a Python virtual environment to run the
# scripts.
# 
# Source this script i.e.
# $ . get_data_create_env.sh
# 
# NB: This script works in the bash shell and requires
# Python 3, wget and unzip

# Download data
wget https://data.dtu.dk/ndownloader/articles/14618094/versions/1
mv 1 data.zip
unzip data.zip

# Set up environment
python3 -m venv data-env
. data-env/bin/activate
pip install -r requirements.txt

echo 'Done!'
echo 'You can now run the scripts with e.g. python barriers.py'

