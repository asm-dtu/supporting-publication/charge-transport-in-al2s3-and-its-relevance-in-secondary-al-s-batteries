# © [2021] Technical University of Denmark

import numpy as np
from ase.units import kB, _e
import matplotlib.pyplot as plt
from formation_energies import get_formation_energies, get_neutral_point, get_ef_ref_to_vbm, sites_per_volume, get_index_closest_to
from barriers import get_barrier

vbm = 2.700
T = 298.15
kBT = kB * T

def get_conductivity(Ef, Eb, q, T=298.15):
    mu = 1e13
    Dx = sites_per_volume()
    # a is the hopping distance in cm
    z_vec = 17.847e-8  # in cm
    a = z_vec / 6  # six equivalent sites in the z direction

    D = mu * a**2 * np.exp(-Eb / kBT)
    mobility = D * q / kBT
    conc = Dx * np.exp(-Ef / kBT)
    sigma = q * conc * mobility * _e
    return sigma

def plot_overpotential():

    e_forms = get_formation_energies()
    jmin = get_neutral_point(e_forms)
    ef = get_ef_ref_to_vbm() - vbm
    fermi_level = ef[jmin]

    overpot = ef - fermi_level
    fig = plt.figure(figsize=(5, 5))
    ax = plt.gca()
    i0 = get_index_closest_to(overpot, 0)

    # S 2- interstitial
    Eb = get_barrier('S_int_-2')

    sigma = get_conductivity(e_forms['S_int_-2'], Eb, q=2)
    # sigma_high = get_conductivity(e_forms['S_int_-2'], Eb, q=2, T=T + 25)

    low_cross = get_index_closest_to(sigma, 1e-11)
    # high_cross = get_index_closest_to(sigma_high, 1e-11)
    print('Overpotential at {0:.2f} K: {1:.2f} V'.format(T, overpot[low_cross]))
    # print('Overpotential decrease at RT + 25: {0:.2f} V'.format(overpot[low_cross] - overpot[high_cross]))

    ax.plot(overpot[i0:], sigma[i0:], 'y-')
    # ax.plot(overpot[i0:], sigma_high[i0:], 'y-', alpha=0.4)
    i = get_index_closest_to(overpot, .9)
    ax.annotate('$S_{i}^{2-}$',
                 (overpot[i], sigma[i]),
                **{'va': 'top', 'ha': 'left'})


    # S 1- interstitial
    Eb = get_barrier('S_int_-1')
    sigma = get_conductivity(e_forms['S_int_-1'], Eb, q=1)
    # sigma_high = get_conductivity(e_forms['S_int_-1'], Eb, q=1, T=T + 25)

    ax.plot(overpot[i0:], sigma[i0:], 'y-')
    # ax.plot(overpot[i0:], sigma_high[i0:], 'y-', alpha=0.4)

    i = get_index_closest_to(overpot, .75)
    ax.annotate('$S_{i}^{1-}$',
                 (overpot[i], sigma[i]),
                **{'va': 'top', 'ha': 'left'})


    # Hole polaron
    Eb = get_barrier('hole_+1')
    sigma = get_conductivity(e_forms['hole_+1'], Eb, q=1)
    # sigma_high = get_conductivity(e_forms['hole_+1'], Eb, q=1, T=T + 25)

    ax.plot(overpot[:i0], sigma[:i0], 'r-.')
    # ax.plot(overpot[:i0], sigma_high[:i0], 'r-.', alpha=0.4)

    i = get_index_closest_to(overpot, -.75)
    ax.annotate('$p^{+}$',
                (overpot[i], sigma[i]),
                **{'va': 'top', 'ha': 'right'})



    plt.yscale('log')
    plt.axis([-1.5, 1.5, 1e-50, 1])
    plt.axvline(0, color='k', linestyle='dashed', lw=1.5)
    # plt.plot(.1, 1e10, 'k', marker=r'$\rightarrow$', markersize=30)
    # plt.text(.1, 1e10, r'$\eta_{\text{chg}}$', anchor='bottom left')
    ax.annotate(r'$\eta_{\mathrm{chg}}$', fontsize=20,
                xy=(.85, .9), xycoords='axes fraction', 
                xytext=(0.55, 0.9), textcoords='axes fraction',
                arrowprops=dict(facecolor='black', shrink=0.05),
                horizontalalignment='left', verticalalignment='center')
    ax.annotate(r'$\eta_{\mathrm{dis}}$', fontsize=20,
                xy=(.15, .9), xycoords='axes fraction', 
                xytext=(0.45, 0.9), textcoords='axes fraction',
                arrowprops=dict(facecolor='black', shrink=0.05),
                horizontalalignment='right', verticalalignment='center')
    sigma_goal = np.ones(len(overpot)) * 1e-11
    ax.plot(overpot, sigma_goal)
    ax.fill_between(overpot, sigma_goal * 1e2, sigma_goal * 1e-2, alpha=0.2)
    ax.set_ylabel('Conductivity / S/cm')
    ax.set_xlabel('U - U$_0$ / V')

    plt.show()

    
if __name__ == '__main__':
    plot_overpotential()
