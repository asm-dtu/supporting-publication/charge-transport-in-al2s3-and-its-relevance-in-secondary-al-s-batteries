# © [2021] Technical University of Denmark

from ase.units import kB
import matplotlib.pyplot as plt
import numpy as np

from ase.db import connect  

db = connect('structures.db')

T = 298.15
npoints = 10000
# ef should be referenced to the valence band maximum (vbm). We
# take E-fermi as reported in the OUTCAR of the neutral reference
# calculation.
vbm = 2.700

def get_ef_ref_to_vbm(npoints=npoints, vbm=vbm):
    # Fermi energy
    ef = np.linspace(0, 4.5, npoints)
    ef_ref_to_vbm = ef + vbm
    return ef_ref_to_vbm

def get_ref_energies():
    # Reference energies
    Al_row = db.get(name='Al_bulk_reference')
    E_Al = Al_row.energy / Al_row.natoms
    S_row = db.get(name='S_bulk_reference')
    E_S = S_row.energy / S_row.natoms
    return E_Al, E_S


def sites_per_volume():
    row = db.get(name='Neutral_bulk_reference')
    # 24 Equivalent sites in the supercell
    Dx = (24 / row.volume) * 1e24  # in cm^-3
    return Dx

def get_formation_energies(npoints=npoints, verbose=False):
    ef_ref_to_vbm = get_ef_ref_to_vbm(npoints)
    E_Al, E_S = get_ref_energies()

    # Get all the defect energies
    E_bulk = db.get(name='Neutral_bulk_reference').energy
    
    # Hole polaron formation energy
    row = db.get(name='Localized_hole')  # Representative localized hole
    # charge correction is already added to the energy
    E_f_hole1 = row.energy - E_bulk + 1 * ef_ref_to_vbm

    # Double hole polaron localized
    E_f_hole2 = db.get(name='Localized_double_hole').energy - E_bulk + 2 * ef_ref_to_vbm

    # Values required for vacancies and defects
    cell_voltage = 1.251  # From formation enthalpies: -724kJ/mol on wikipedia
    chem_pot_Al = E_Al - cell_voltage
    chem_pot_S = E_S
    
    # Al vacancies
    E_f_Al_vac = {}
    for nc in [0, 1, 2, 3]:
        # nc is the net charge set in vasp
        Al_row = db.get(name=f'Al_{nc}-_vacancy')
        E_f_Al_vac[nc] = Al_row.energy - E_bulk + 1 * chem_pot_Al + (-nc) * ef_ref_to_vbm

    # S vacancies
    E_f_S_vac = {}
    for nc in [0, -1, -2]:
        S_row = db.get(name=f'S_{-nc}+_vacancy')
        E_f_S_vac[nc] = S_row.energy - E_bulk + 1 * chem_pot_S + (-nc) * ef_ref_to_vbm

    # Al interstitials
    E_f_Al_int = {}
    for nc in [0, -1, -2, -3]:
        Al_row = db.get(name=f'Al_{-nc}+_interstitial')
        E_f_Al_int[nc] = Al_row.energy - E_bulk - 1 *  chem_pot_Al + (-nc) * ef_ref_to_vbm

    # S interstitials
    E_f_S_int = {}
    for nc in [0, 1, 2]:
        S_row = db.get(name=f'S_{nc}-_interstitial')
        E_f_S_int[nc] = S_row.energy - E_bulk - 1 * chem_pot_S + (-nc) * ef_ref_to_vbm

    formation_energies = {'hole_+1': E_f_hole1, 'hole_+2': E_f_hole2}
    formation_energies.update(dict((f'Al_vac_-{q}', E_f_Al_vac[q]) for q in [0, 1, 2, 3]))
    formation_energies.update(dict((f'S_vac_+{q}', E_f_S_vac[-q]) for q in [0, 1, 2]))
    formation_energies.update(dict((f'Al_int_+{q}', E_f_Al_int[-q]) for q in [0, 1, 2, 3]))
    formation_energies.update(dict((f'S_int_-{q}', E_f_S_int[q]) for q in [0, 1, 2]))
    return formation_energies

def get_neutral_point(formation_energies, T=T):
    kBT = kB * T
    # Using the same Dx for all species.
    Dx = sites_per_volume()
    
    positive_charge = 0
    negative_charge = 0
    for k, e_form in formation_energies.items():
        sign = k[-2]
        q = int(k[-1])
        concentration = Dx * np.exp(-e_form / kBT)
        
        if sign == '+':
            positive_charge += q * concentration
        elif sign == '-':
            negative_charge += q * concentration
        else:
            print('Formation energy has an unworkable name')
        
    # Applying charge neutrality condition
    jmin = np.argmin(np.abs(positive_charge - negative_charge))
    return jmin

def get_index_closest_to(arr, x):
    return np.argmin(np.abs(arr - x))

def plot_formation_energies(formation_energies, ef):
    fig = plt.figure(figsize=(5, 5))
    jmin = get_neutral_point(formation_energies)
    ax = plt.gca()
    
    # plot the hole formation energy as a function of fermi energy
    ax.plot(ef, formation_energies['hole_+1'], 'r-.')

    # Where do we want the annotation to be situated on the x-axis
    x_loc = .5
    i = get_index_closest_to(ef, x_loc)
    ax.annotate('$p^{+}$', (ef[i], formation_energies['hole_+1'][i]),
                 va='bottom', ha='right')

    # plot the double hole formation energy as a function of fermi energy
    ax.plot(ef, formation_energies['hole_+2'], 'r-.')
    x_loc = .5
    i = get_index_closest_to(ef, x_loc)
    ax.annotate('$p^{2+}$', (ef[i],formation_energies['hole_+2'][i]),
                 va='center', ha='left')

    # Al vacancies
    locs = {'x': {1: 4, 3: 3.5},
            'loc': {0: {'va': 'bottom', 'ha': 'center'},}}
    for nc in [0, 1, 2, 3]:
        ax.plot(ef, formation_energies[f'Al_vac_-{nc}'], 'b--')
        x_loc = locs['x'].get(nc, 3.3)
        i = get_index_closest_to(ef, x_loc)
        ax.annotate('$V_{Al}^{' + str(nc) + '- '[nc == 0] + '}$',
                     (ef[i], formation_energies[f'Al_vac_-{nc}'][i]),
                     **locs['loc'].get(nc, {'va': 'bottom', 'ha': 'left'}))

    # S vacancies
    locs = {'x': {0: 1.2, -2: .5},
            'loc': {0: {'va': 'bottom', 'ha': 'center'},}}
    for nc in [0, -1, -2]:
        ax.plot(ef, formation_energies[f'S_vac_+{-nc}'], 'y--')
        x_loc = locs['x'].get(nc, .65)
        i = get_index_closest_to(ef, x_loc)
        ax.annotate('$V_{S}^{' + str(abs(nc)) + '+ '[nc == 0] + '}$',
                     (ef[i], formation_energies[f'S_vac_+{-nc}'][i]),
                     **locs['loc'].get(nc, {'va': 'bottom', 'ha': 'right'}))

    # Al interstitials
    locs = {'x': {-3: 1.1, -2: .5, -1: 1.15},
            'loc': {-3: {'va': 'top', 'ha': 'left'},
                    -2: {'va': 'top', 'ha': 'left'},
                    -1: {'va': 'bottom', 'ha': 'right'}}}
    for nc in [0, -1, -2, -3]:
        ax.plot(ef, formation_energies[f'Al_int_+{-nc}'], 'b-')
        x_loc = locs['x'].get(nc, 1.05)
        i = get_index_closest_to(ef, x_loc)
        ax.annotate('$Al_{i}^{' + str(abs(nc)) + '+ '[nc == 0] + '}$',
                    (ef[i], formation_energies[f'Al_int_+{-nc}'][i]),
                    **locs['loc'].get(nc, {'va': 'bottom', 'ha': 'right'}))

    # S interstitials
    locs = {'x': {0: 4.2, 2: 3},
            'loc': {2: {'va': 'top', 'ha': 'right'},}}
    for nc in [0, 1, 2]:
        ax.plot(ef, formation_energies[f'S_int_-{nc}'], 'y-')
        x_loc = locs['x'].get(nc, 2.5)
        i = get_index_closest_to(ef, x_loc)
        ax.annotate('$S_{i}^{' + str(abs(nc)) + '- '[nc == 0] + '}$',
                     (ef[i], formation_energies[f'S_int_-{nc}'][i]),
                     **locs['loc'].get(nc, {'va': 'bottom', 'ha': 'left'}))


    ax.vlines(ef[jmin], 0, 4.5, linestyles='dashed', lw=.5)

    plt.axis('scaled')
    plt.axis([0, 4.5, 0, 4.5])
    ax.set_ylabel('Formation energy / eV')
    ax.set_xlabel('Fermi energy / eV')
    plt.show()
    

if __name__ == "__main__":
    formation_energies = get_formation_energies(10000)
    jmin = get_neutral_point(formation_energies)

    ef = get_ef_ref_to_vbm() - vbm
    print(f'Fermi level: {ef[jmin]:.2f} eV')
    for k, e_form in formation_energies.items():
        print(f'Formation energy of {k}: {e_form[jmin]:.2f}')

    plot_formation_energies(formation_energies, ef)
