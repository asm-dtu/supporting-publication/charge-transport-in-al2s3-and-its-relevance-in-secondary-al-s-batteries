# Charge transport in Al2S3 and its relevance in secondary Al-S batteries

Scripts that support the publication Charge transport in Al2S3 and its relevance in secondary Al-S batteries by Steen Lysgaard and Juan Marı́a Garcı́a Lastra. The Journal of Physical Chemistry C, 125(30), 16444–16450.

## Info

The publication is at:

https://doi.org/10.1021/acs.jpcc.1c04484


The data is located at:

https://doi.org/10.11583/DTU.14618094

## How to use the scripts
Download the scripts and then download the data from [DTU data](https://doi.org/10.11583/DTU.14618094) and put both data and scripts in the same folder. This process is automated by using `get_data_create_env.sh`.

- `formation_energies.py` plots the formation energies of all calculated defects
- `localization_energy.py` prints the localization energy of a hole polaron
- `barriers.py` will plot the energy barriers of the migration paths for the three relevant defects.
- `overpotential.py` makes the overpotential plot showing the increasing conductivity as a function overpotential

## Requirements
- [Python](https://www.python.org/)
- [Numpy](https://numpy.org)
- [Matplotlib](https://matplotlib.org/)
- [ASE](https://wiki.fysik.dtu.dk/ase/ase/ase.html)
