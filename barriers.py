# © [2021] Technical University of Denmark

import matplotlib.pyplot as plt
import os

from ase.io import read
from ase.neb import NEBTools
from ase.utils.forcecurve import fit_images

paths = {'S_int_-2': 'S_int_-2_path.traj', 'S_int_-1': 'S_int_-1_path.traj', 'hole_+1': 'hole_path.traj'}

def get_barrier(defect):
    images = read(paths[defect], index=':')
    nt = NEBTools(images)
    return nt.get_barrier()[0]

def plot_normed_barrier(forcefit, ax=None, leg=None):
    symbols = ['o', '^', 's']
    
    ff = forcefit
    if ax is None:
        ax = plt.gca()

    dist = ff.path[-1]
    normed_path = ff.path / dist
    normed_fit_path = ff.fit_path / dist
    symbol = symbols[len(ax.lines) // 2]
    ax.plot(normed_path, ff.energies, symbol, label=leg)
    ax.plot(normed_fit_path, ff.fit_energies, 'k-')
    ax.set_xlabel(r'Normalized path / a.u.')
    ax.set_ylabel('energy / eV')
    ax.set_xlim([-0.05, 1.05])
    return ax

def plot_barriers():
    images_hole = read(paths['hole_+1'], index=':')
    ff = fit_images(images_hole)
    ax = plot_normed_barrier(ff, leg='p$^{+}$')

    images_S1_int = read(paths['S_int_-1'], index=':')
    ff = fit_images(images_S1_int)
    ax = plot_normed_barrier(ff, ax=ax, leg='S$^{1-}$')

    images_S2_int = read(paths['S_int_-2'], index=':')
    ff = fit_images(images_S2_int)
    ax = plot_normed_barrier(ff, ax=ax, leg='S$^{2-}$')

    fig = plt.gcf()
    fig.legend(loc='upper right')
    fig.tight_layout()

    plt.show()

if __name__ == "__main__":
    plot_barriers()
