# © [2021] Technical University of Denmark

from ase.db import connect  

db = connect('structures.db')

delocalized = db.get(name='Delocalized_hole').energy
localized = db.get(name='Localized_hole').energy

loc_en = localized - delocalized

print(f'Localization energy: {loc_en:.2f} eV')
